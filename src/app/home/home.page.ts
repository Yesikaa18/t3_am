import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GaleriaImgPage } from '../galeria-img/galeria-img.page';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private modalCtrl: ModalController){
  }
  imagenes = [ // Arreglo de imagenes que utilizaré
    'assets/Galeria/img1.jpeg',
    'assets/Galeria/img2.jpeg',
    'assets/Galeria/img3.jpeg',
    'assets/Galeria/img4.jpeg',
    'assets/Galeria/img5.jpeg',
    'assets/Galeria/img6.jpeg',
    'assets/Galeria/img7.jpeg',
    'assets/Galeria/img8.jpeg',
    'assets/Galeria/img9.jpeg',
    'assets/Galeria/img10.jpeg',
    'assets/Galeria/img11.jpeg',
    'assets/Galeria/img12.jpeg',
    'assets/Galeria/img13.jpeg',
    'assets/Galeria/img14.jpeg'
  ];

  zoomImg(index){ // función para abrir imagen
    this.modalCtrl.create({
      component : GaleriaImgPage,
      componentProps: {
      ind: index
      }
    // tslint:disable-next-line:semicolon
    }).then(modal => modal.present())
  }



}
