import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GaleriaImgPageRoutingModule } from './galeria-img-routing.module';

import { GaleriaImgPage } from './galeria-img.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GaleriaImgPageRoutingModule
  ],
  declarations: [GaleriaImgPage]
})
export class GaleriaImgPageModule {}
