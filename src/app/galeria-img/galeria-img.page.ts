import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-galeria-img',
  templateUrl: './galeria-img.page.html',
  styleUrls: ['./galeria-img.page.scss'],
})
export class GaleriaImgPage implements OnInit {

  index: number;
  @ViewChild('slides', {static: true}) slides: IonSlides;
  viewEntered = false;
  imagenes = [  // Arreglo de imagenes que utilizaré
    'assets/Galeria/img1.jpeg',
    'assets/Galeria/img2.jpeg',
    'assets/Galeria/img3.jpeg',
    'assets/Galeria/img4.jpeg',
    'assets/Galeria/img5.jpeg',
    'assets/Galeria/img6.jpeg',
    'assets/Galeria/img7.jpeg',
    'assets/Galeria/img8.jpeg',
    'assets/Galeria/img9.jpeg',
    'assets/Galeria/img10.jpeg',
    'assets/Galeria/img11.jpeg',
    'assets/Galeria/img12.jpeg',
    'assets/Galeria/img13.jpeg',
    'assets/Galeria/img14.jpeg'

  ];

  constructor(private modalCtrl: ModalController, private navParams: NavParams, private loadingCtrl: LoadingController) {

    this.loadingCtrl.create({ // mensaje de "Cargando imagen" al darle click a una imagen de la galería
      message: 'Cargando imagen',
      duration: 1000
    }).then(loading => loading.present());

    this.index = this.navParams.get('ind');

  // tslint:disable-next-line:semicolon
    console.log(this.index)
}
  ionViewDidEnter() {
     this.viewEntered = true;
    }

  ngOnInit() {
  }

  closeModal(){ // Cerrar imagen
    this.modalCtrl.dismiss();
  }
}
