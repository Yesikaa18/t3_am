import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GaleriaImgPage } from './galeria-img.page';

const routes: Routes = [
  {
    path: '',
    component: GaleriaImgPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GaleriaImgPageRoutingModule {}
